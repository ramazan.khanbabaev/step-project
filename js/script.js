document.addEventListener('DOMContentLoaded', () => {
// Our Services tabs
  document.querySelector('.services-tabs').addEventListener('click', event => {
    document.querySelector('.services-tab-title.active').classList.remove('active');
    event.target.classList.add('active');

    document.querySelector('.services-tab-content.active').classList.remove('active');
    document.querySelector('.services-tab-content[data-tab-title="' + event.target.dataset.tabTitle + '"]').classList.add('active');
  });

// Our Amazing Work images
  const imgs = {
    graphicDesign: [
      'img/graphic-design/graphic-design1.jpg',
      'img/graphic-design/graphic-design2.jpg',
      'img/graphic-design/graphic-design3.jpg',
      'img/graphic-design/graphic-design4.jpg',
      'img/graphic-design/graphic-design5.jpg',
      'img/graphic-design/graphic-design6.jpg',
      'img/graphic-design/graphic-design7.jpg',
      'img/graphic-design/graphic-design8.jpg',
      'img/graphic-design/graphic-design9.jpg',
      'img/graphic-design/graphic-design10.jpg',
      'img/graphic-design/graphic-design11.jpg',
      'img/graphic-design/graphic-design12.jpg',
    ],
    webDesign: [
      'img/web-design/web-design1.jpg',
      'img/web-design/web-design2.jpg',
      'img/web-design/web-design3.jpg',
      'img/web-design/web-design4.jpg',
      'img/web-design/web-design5.jpg',
      'img/web-design/web-design6.jpg',
      'img/web-design/web-design7.jpg',
    ],
    landingPages: [
      'img/landing-page/landing-page1.jpg',
      'img/landing-page/landing-page2.jpg',
      'img/landing-page/landing-page3.jpg',
      'img/landing-page/landing-page4.jpg',
      'img/landing-page/landing-page5.jpg',
      'img/landing-page/landing-page6.jpg',
      'img/landing-page/landing-page7.jpg',
    ],
    wordpress: [
      'img/wordpress/wordpress1.jpg',
      'img/wordpress/wordpress2.jpg',
      'img/wordpress/wordpress3.jpg',
      'img/wordpress/wordpress4.jpg',
      'img/wordpress/wordpress5.jpg',
      'img/wordpress/wordpress6.jpg',
      'img/wordpress/wordpress7.jpg',
      'img/wordpress/wordpress8.jpg',
      'img/wordpress/wordpress9.jpg',
      'img/wordpress/wordpress10.jpg',
    ]
  };
  imgs.all = [...imgs.graphicDesign, ...imgs.webDesign, ...imgs.landingPages, ...imgs.wordpress].sort(() => Math.random() - 0.5);

  const categoryTitle = document.querySelector('.work-images-item-hover-subtitle');
  const hoverBlock = document.querySelector('.work-images-item-hover');
  hoverBlock.remove();
  const preloader = document.querySelector('.preloader');
  preloader.remove();

  const imgsContainer = document.querySelector('.work-images');
  const loadMoreBtn = document.querySelector('.load-more-btn');

  // Hover
  imgsContainer.addEventListener('mouseover', event => {
    if (!event.target.parentElement.contains(hoverBlock)){
      event.target.parentElement.appendChild(hoverBlock);
    }
  });

  // Create image with wrapper
  const img = document.createElement('img');
  img.alt = 'Our Amazing Work image';
  img.className = 'work-images-item-img';
  const imgItem = document.createElement('div');
  imgItem.className = 'work-images-item';
  imgItem.appendChild(img);

  const countImgs = imgClassName => document.getElementsByClassName(imgClassName).length;
  const toggleLoadBtn = (btn, img, imgSource, imgCategory, imgsWrapper) => {
    const imgsOnPage = countImgs(img.className);
    if (imgsOnPage < imgSource[imgCategory].length) {
      imgsWrapper.after(btn);
    } else {
      btn.remove();
    }
  };

  // Function for adding images
  const showImgs = (img, imgSource, imgCategory, imgsWrapper) => {
    const imgsOnPage = countImgs(img.className);
    for (let i = imgsOnPage, length = imgSource[imgCategory].length; i < imgsOnPage + 12 && i < length; i++) {
      img.src = imgSource[imgCategory][i];
      imgsWrapper.appendChild(img.parentElement.cloneNode(true));
    }
  };

  const removeImgs = () => document.querySelectorAll('.work-images-item').forEach(img => img.remove());

  // Add first 12 images
  const activeTabTitle = document.querySelector('.work-tab-title.active').dataset.tabTitle;
  showImgs(img, imgs, activeTabTitle, imgsContainer);
  toggleLoadBtn(loadMoreBtn, img, imgs, activeTabTitle, imgsContainer);

  // Switch tab
  document.querySelector('.work-tabs').addEventListener('click', event => {
    document.querySelector('.work-tab-title.active').classList.remove('active');
    event.target.classList.add('active');

    removeImgs();
    showImgs(img, imgs, event.target.dataset.tabTitle, imgsContainer);
    toggleLoadBtn(loadMoreBtn, img, imgs, event.target.dataset.tabTitle, imgsContainer);
    categoryTitle.innerText = event.target.innerText;
  });

  // Load more button
  loadMoreBtn.addEventListener('click', event => {
    const activeTabTitle = document.querySelector('.work-tab-title.active').dataset.tabTitle;
    imgsContainer.after(preloader);
    setTimeout(()=> {
      preloader.remove();
      showImgs(img, imgs, activeTabTitle, imgsContainer);
      toggleLoadBtn(loadMoreBtn, img, imgs, activeTabTitle, imgsContainer);
    }, 2000);
  });

  // What People Say About theHam
  const bigIcon = document.querySelector('.people-chosen');
  const switchIcon = (previousPerson, nextPerson) => {
    bigIcon.removeChild(bigIcon.firstElementChild);
    bigIcon.appendChild(nextPerson.cloneNode());
    previousPerson.classList.remove('active');
    nextPerson.classList.add('active');

    document.querySelector('.people-comment-text.active').classList.remove('active');
    document.querySelector(`.people-comment-text[data-name="${nextPerson.dataset.name}"]`).classList.add('active');

    document.querySelector('.people-name.active').classList.remove('active');
    document.querySelector(`.people-name[data-name="${nextPerson.dataset.name}"]`).classList.add('active');

    document.querySelector('.people-post.active').classList.remove('active');
    document.querySelector(`.people-post[data-name="${nextPerson.dataset.name}"]`).classList.add('active');
  };

  // Click on icon
  document.querySelector('.people-chosen-nav-persons').addEventListener('click', event => {
    const previousPerson = document.querySelector('.people-persons-item-img.active');
    if (event.target !== event.currentTarget) {
      switchIcon(previousPerson, event.target)
    }
  });

  // Click on back button
  document.querySelector('.people-chosen-nav-back').addEventListener('click', event => {
    const previousPerson = document.querySelector('.people-persons-item-img.active');
    const nextPerson = (previousPerson === previousPerson.parentElement.firstElementChild)
        ? previousPerson.parentElement.lastElementChild
        : previousPerson.previousElementSibling;
    switchIcon(previousPerson, nextPerson)
  });

  // Click on forward button
  document.querySelector('.people-chosen-nav-forward').addEventListener('click', event => {
    const previousPerson = document.querySelector('.people-persons-item-img.active');
    const nextPerson = (previousPerson === previousPerson.parentElement.lastElementChild)
        ? previousPerson.parentElement.firstElementChild
        : previousPerson.nextElementSibling;
    switchIcon(previousPerson, nextPerson)
  });

  // Masonry (Gallery of the best images)
  const galleryImgs = {
    all: [
    'img/gallery/road.jpg',
    'img/gallery/boy.jpg',
    'img/gallery/flowers.jpg',
    'img/gallery/girl.jpg',
    'img/gallery/iphone.jpg',
    'img/gallery/love-hands.jpg',
    'img/gallery/love-tree.jpeg',
    'img/gallery/rails.jpg'
  ]};
  galleryImgs.all.push(...imgs.all);

  const galleryImgsContainer = document.querySelector('.gallery-photos');
  const galleryLoadMoreBtn = document.querySelector('.gallery-load-more-btn');
  const galleryHoverBlock = document.querySelector('.gallery-photos-item-hover');
  galleryHoverBlock.remove();

  // Hover
  galleryImgsContainer.addEventListener('mouseover', event => {
    if (!event.target.parentElement.contains(galleryHoverBlock)){
      event.target.parentElement.appendChild(galleryHoverBlock);
    }
  });

  // Masonry on first block
  imagesLoaded(galleryImgsContainer, () => {
    new Masonry(document.querySelector('.gallery-photos-item'), {
      itemSelector: '.gallery-photos-package-item',
      columnWidth: 185,
      gutter: 7
    });
  });

  // Create image with wrapper
  const galleryImg = document.createElement('img');
  galleryImg.alt = 'Gallery photo';
  galleryImg.className = 'gallery-photos-item-img';
  const galleryImgItem = document.createElement('div');
  galleryImgItem.className = 'gallery-photos-item';
  galleryImgItem.appendChild(galleryImg);

  // Add images and masonry in section
  const showGalleryImgs = () => {
    showImgs(galleryImg, galleryImgs, 'all', galleryImgsContainer);
    toggleLoadBtn(galleryLoadMoreBtn, galleryImg, galleryImgs, 'all', galleryImgsContainer);
    imagesLoaded(galleryImgsContainer, () => {
      new Masonry(galleryImgsContainer, {
        itemSelector: '.gallery-photos-item',
        isFitWidth: true,
        gutter: 5
      });
    })
  };
  showGalleryImgs();
  galleryLoadMoreBtn.addEventListener('click', () => {
    galleryLoadMoreBtn.before(preloader);
    setTimeout(()=> {
      preloader.remove();
      showGalleryImgs()
    }, 2000);
  });
});